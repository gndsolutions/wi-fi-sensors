/*
 * Copyright 2015, GND Solutions India Pvt Ltd
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of GND Solutions;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of GND Solutions.
 */

#include "wiced.h"
#include "stm32f2xx_i2c.h"
#include "stm32f2xx.h"
#include "temp_sensor.h"

/******************************************************
 *                    Structures
 ******************************************************/
static wiced_i2c_device_t temp_sensor_device = {
        .port = WICED_I2C_1,
        .address = 0x18,
        .address_width = I2C_ADDRESS_WIDTH_7BIT,
        .flags = I2C_DEVICE_NO_DMA,
        .speed_mode = I2C_STANDARD_SPEED_MODE
};

/******************************************************
 *                 Type Definitions
 ******************************************************/
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned long uint32_t;

/******************************************************
 *               Function Definitions
 ******************************************************/
wiced_result_t init_temp_sensor()
{
    WPRINT_APP_INFO(("Initializing Temperature sensor\n"));

    wiced_i2c_init( &temp_sensor_device );

    if ( wiced_i2c_probe_device( &temp_sensor_device, 3 ) != WICED_TRUE )
    {
        return WICED_ERROR;
    }

    return WICED_SUCCESS;
}

uint16_t i2c_read(void)
{
    uint16_t x;

    wiced_i2c_message_t i2c_message;
    uint8_t i2c_tx_buff;
    uint8_t i2c_rx_buff[2];

    i2c_message.tx_buffer = &i2c_tx_buff;
    i2c_message.tx_length = 0;
    i2c_message.rx_buffer = &i2c_rx_buff;
    i2c_message.rx_length = 2;
    memset(&i2c_rx_buff, 0x00, sizeof(i2c_rx_buff));

    wiced_i2c_transfer (&temp_sensor_device, &i2c_message,1);

    x = i2c_rx_buff[0];
    x <<= 8;
    x |= i2c_rx_buff[1];

    return (x);
}

void i2c_write(uint8_t reg)
{
    wiced_i2c_message_t i2c_message;
    uint8_t i2c_tx_buff;

    i2c_message.tx_buffer = &i2c_tx_buff;
    i2c_message.tx_length = 1;
    i2c_tx_buff = reg;
    i2c_message.retries = 5;

    wiced_i2c_transfer (&temp_sensor_device, &i2c_message,1);
}

uint16_t read_data(uint8_t reg)
{
    uint16_t version;

    i2c_write(DEVICE_ADDR & 0xFE);
    i2c_write(reg);
    i2c_write(DEVICE_ADDR | 0x01);
    version = i2c_read();

    return(version);
}

void write_data(uint8_t reg, uint16_t data)
{
    i2c_write(DEVICE_ADDR & 0xFE);
    i2c_write(reg);
    i2c_write(data>>8);
    i2c_write(data & 0x00FF);
}

uint8_t temp_calc(void)
{
    uint16_t temp;
    uint8_t temperature;

    temp = read_data(TEMP_REGISTER);

    temperature = (((temp >>8) << 4) & 0xF0) | (((temp & 0x00FF) >> 4) & 0x0F);

    return(temperature);
}

