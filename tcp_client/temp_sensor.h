/*
 * Copyright 2015, GND Solutions India Pvt Ltd
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of GND Solutions;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of GND Solutions.
 */

/******************************************************
 *                      Macros
 ******************************************************/

#define DEVICE_ADDR                              0x30  //depends on connection of A0,A1,A2 pins

#define CONFIG_REGISTER                          0x01
#define TEMP_UPPER_BOUND                         0x02
#define TEMP_LOWER_BOUND                         0x03
#define CRITICAL_TEMP                            0x04
#define TEMP_REGISTER                            0x05
#define MANUFACTURER_ID                          0x06
#define DEVICE_ID                                0x07
#define RESOL_REG                                0x08

/******************************************************
 *               Function Declarations
 ******************************************************/
wiced_result_t init_temp_sensor();
void i2c_write(uint8_t reg);
void i2c16_write(uint16_t reg);
uint16_t i2c_read(void);
uint16_t read_data(uint8_t reg);
void write_data(uint8_t reg, uint16_t data);
uint8_t temp_calc(void);

